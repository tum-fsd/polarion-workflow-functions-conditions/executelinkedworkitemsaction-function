package com.tulrfsd.polarion.workflow.functions.executelinkedworkitemsaction;

import com.polarion.alm.tracker.internal.model.WorkItem;
import com.polarion.alm.tracker.model.IStatusOpt;
import com.polarion.alm.tracker.model.IWorkItem;
import com.polarion.alm.tracker.model.IWorkflowAction;
import com.polarion.alm.tracker.workflow.IArguments;
import com.polarion.alm.tracker.workflow.ICallContext;
import com.polarion.alm.tracker.workflow.IFunction;
import com.polarion.core.util.exceptions.UserFriendlyRuntimeException;
import com.polarion.core.util.logging.Logger;
import com.polarion.platform.core.PlatformContext;
import com.polarion.platform.i18n.Localization;
import com.polarion.platform.security.ISecurityService;
import com.polarion.platform.security.PermissionDeniedException;
import java.security.PrivilegedAction;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

@SuppressWarnings("restriction")
public class ExecuteLinkedWorkItemsAction implements IFunction<IWorkItem> {

  private static final Logger logger = Logger.getLogger(ExecuteLinkedWorkItemsAction.class);

  private static ISecurityService securityService = PlatformContext.getPlatform().lookupService(ISecurityService.class);
  
  private Set<String> linkRolesDirect;
  private Set<String> linkRolesBack;
  private Set<String> originStates;
  private boolean originNegate;
  private String targetState;
  private Set<String> workItemTypeIds;
  private String actionId;
  private boolean followLinks;
  private IWorkItem baseWorkItem;
  
  public void execute(ICallContext<IWorkItem> context, IArguments arguments) {

    linkRolesDirect = arguments.getAsSetOptional("link.roles.direct");
    linkRolesBack = arguments.getAsSetOptional("link.roles.back");
    if (linkRolesDirect.isEmpty() && linkRolesBack.isEmpty()) {
      throw new UserFriendlyRuntimeException(Localization.getString("form.workitem.message.parametersMustBeSet", "link.roles.back"));
    }
    originStates = arguments.getAsSet("origin.states");
    originNegate = arguments.getAsBoolean("origin.negate", false);
    targetState = arguments.getAsString("target.state");
    workItemTypeIds = arguments.getAsSetOptional("workitem.type.ids");
    actionId = arguments.getAsString("action.id");
    followLinks = arguments.getAsBoolean("follow.links", false);

    baseWorkItem = context.getTarget();
      
    Set<IWorkItem> workItemsToUpdate = securityService.doAsSystemUser((PrivilegedAction<Set<IWorkItem>>) this::getWorkItemsToUpdate);
    updateWorkItems(workItemsToUpdate);
  }

  private Set<IWorkItem> getWorkItemsToUpdate() {
    Set<IWorkItem> linkedWorkItems = new HashSet<>();
    
    IWorkItem.ITerminalCondition condition = wi -> baseWorkItem.equals(wi);
    
    // detect possible loop first
    boolean loopDetected = ((WorkItem) baseWorkItem).traverseLinkedWorkitems(linkedWorkItems, linkRolesDirect, linkRolesBack, condition, true);
    linkedWorkItems.clear();
    if(loopDetected) {
      throw new UserFriendlyRuntimeException("A possible linked work item loop was detected for work item " + baseWorkItem.getId() + ". The operation is canceled to prevent an infinite loop crashing the server.");
    }
    // get deep or flat linked work items
    ((WorkItem) baseWorkItem).traverseLinkedWorkitems(linkedWorkItems, linkRolesDirect, linkRolesBack, condition, followLinks);
    
    linkedWorkItems.remove(baseWorkItem);
    Set<IWorkItem> result = new LinkedHashSet<>();
    for (IWorkItem linkedWorkItem : linkedWorkItems) {
      if (linkedWorkItem.isUnresolvable() ||
          !(workItemTypeIds.isEmpty() || workItemTypeIds.contains(linkedWorkItem.getType().getId()))) {
        continue;
      }
      IStatusOpt status = linkedWorkItem.getStatus();
      if (status == null || ((originStates.contains(status.getId()) ^ originNegate) && !targetState.equals(status.getId()))) { // ^ is XOR operator
        result.add(linkedWorkItem);
      }
    }
    return result;
  }
  
  private void updateWorkItems(Set<IWorkItem> workItemsToUpdate) {
    
    String cannotExecuteBecauseCannotChangeLinkedMessage = Localization.getString("form.workitem.message.cannotExecuteBecauseCannotChangeLinked");
    
    for (IWorkItem wi : workItemsToUpdate) {
      if (!wi.can().modifyKey("status")) {
        throw new UserFriendlyRuntimeException(cannotExecuteBecauseCannotChangeLinkedMessage + " You do not have the permissions to modify the status of the work item " + wi.getId() + " in the project with the ID \"" + wi.getProjectId() + "\".");
      }
      IWorkflowAction[] availableActions = wi.getAvailableActions();
      boolean actionAvailable = false;
      for (IWorkflowAction workflowAction : availableActions) {
        if (workflowAction.getNativeActionId().equals(actionId) && workflowAction.getTargetStatus().getId().equals(targetState)) {
          try {
            wi.performAction(workflowAction.getActionId());
            wi.save();
          } catch (PermissionDeniedException e) {
            logger.warn(e.getMessage(), e);
            throw new UserFriendlyRuntimeException(cannotExecuteBecauseCannotChangeLinkedMessage);
          }
          actionAvailable = true;
          break;
        }
      }
      if (!actionAvailable) {
        throw new UserFriendlyRuntimeException(cannotExecuteBecauseCannotChangeLinkedMessage + " The action " + actionId + " is not available for work item " + wi.getId() + " in the project " + wi.getProject().getName() + ".");
      }
    }
  }
}
