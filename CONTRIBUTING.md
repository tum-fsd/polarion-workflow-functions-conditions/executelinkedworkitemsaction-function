## Developer Certificate of Origin + License

By contributing to TUM-FSD, You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to TUM-FSD.
Except for the license granted herein to TUM-FSD and recipients of software
distributed by TUM-FSD, You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

### Developer Certificate of Origin

Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
1 Letterman Drive
Suite D4700
San Francisco, CA, 94129

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.

### License

Apache 2.0, see [LICENSE](LICENSE).

# Contribute to the Workflow Function/Condition

This guide details how to use issues and pull requests to improve the extension.

Please stick to the guidelines as close as possible. That way we ensure quality guides and easy to merge requests.

Your Pull Request will be reviewed by one of our devs/volunteers and you will be asked to reformat it if needed.

## Issues

Open an issue if you want to report a bug or suggest a feature.
Use the provided issue templates and fill them out as much as possible

## Merge Request title

Try to be as descriptive as you can in your Merge Request title.

Particularly if you are submitting a new feature or your modifications require a higher minimum version of Polarion.

For example some good titles would be:

* [topic] Polarion 2XRX
* [cool new feature] Polarion 22R1

If your pull request does not require a new tool version, you don't need to provide the versions.